const {
  publicSaleAddress,
  exchangeLiquidityAddress,
  commitSHA,
} = require("../secrets.json");

const { deployProxy } = require("@openzeppelin/truffle-upgrades");

const tokenContract = artifacts.require("PlaycentTokenV1");

module.exports = async function (deployer, network, accounts) {
  await deployProxy(
    tokenContract,
    [publicSaleAddress, exchangeLiquidityAddress, commitSHA],
    {
      deployer,
      initializer: "initialize",
    }
  );
};
